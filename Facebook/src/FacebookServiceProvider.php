<?php

namespace jay\Facebook;

use Illuminate\Support\ServiceProvider;
use jay\Facebook\Facades\Facebook;

class FacebookServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('fbm',function($app){
            return new Facebook(Config('facebook.fb'));
        });
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__."/routes.php");
        $this->publishes([
            __DIR__.'/Config/facebook.php' => config_path('facebook.php'),
        ]);
    }
}
